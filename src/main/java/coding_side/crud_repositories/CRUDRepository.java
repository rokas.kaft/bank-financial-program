package coding_side.crud_repositories;

import coding_side.entity.Income;
import coding_side.exceptions.NoSuchEntityException;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

public interface CRUDRepository<ID extends Serializable, T> {

    void save(T entity);  // CREATE | UPDATE

    Optional<T> findById(ID id);

    default void typeIn(ID id){
        Scanner sc = new Scanner(System.in);
        double number = sc.nextDouble();
    }

    default T getById(ID id) {
        return findById(id).orElseThrow(() -> new NoSuchEntityException((String) id));
    }

    default void delete(ID id) { // DELETE
        delete(getById(id));
    }

    void delete(T entity);

}
