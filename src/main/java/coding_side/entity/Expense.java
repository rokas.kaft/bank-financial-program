package coding_side.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "expense")
public class Expense implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "expense_id")
    private Integer id;

    @Column(name = "expense_category_id")
    private Integer expenseCategoryId;

    @Column (name = "expense_amount", nullable = false)
    private Integer expenseAmount;

    @Column (name = "expense_date", nullable = false)
    private LocalDate expenseDate;

    @ManyToMany(mappedBy = "expenses", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Budget> budget = new HashSet<>();

    @ManyToOne(optional=false)
    @JoinColumn(name = "expense_category_id",insertable=false, updatable=false)
    private ExpenseCategory expenseCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExpenseCategoryId() {
        return expenseCategoryId;
    }

    protected void setExpenseCategoryId(Integer expenseCategoryId) {
    }

    public Integer getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(Integer expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public LocalDate getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(LocalDate expenseDate) {
        this.expenseDate = expenseDate;
    }

    public ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public Set<Budget> getBudget() {
        return budget;
    }

    public void setBudget(Set<Budget> budget) {
        this.budget = budget;
    }

    @Override
    public String toString() {
        return "Expense{" +
                "id=" + id +
                ", expenseCategoryId=" + expenseCategoryId +
                ", expenseAmount=" + expenseAmount +
                ", expenseDate=" + expenseDate +
                ", budget=" + budget +
                ", expenseCategory=" + expenseCategory +
                '}';
    }
}
