package coding_side.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "income_category")

public class IncomeCategory implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "income_category_id")
    private Integer id;

    @Column (name = "income_category_name", nullable = false)
    private String incomeCategoryName;

    @OneToMany(mappedBy = "incomeCategory")
    private List<Income> incomes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIncomeCategoryName() {
        return incomeCategoryName;
    }

    public void setIncomeCategoryName(String incomeCategoryName) {
        this.incomeCategoryName = incomeCategoryName;
    }

    @Override
    public String toString() {
        return "Income_category{" +
                "id=" + id +
                ", incomeCategoryName='" + incomeCategoryName + '\'' +
                ", incomes=" + // incomes +
                '}';
    }
}
