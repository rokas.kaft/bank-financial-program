package coding_side.crud_repositories;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.io.Serializable;
import java.util.*;

public abstract class AbstractRepository<ID extends Serializable, T> implements CRUDRepository {

    protected static EntityManager entityManager = null;

    public AbstractRepository(EntityManager entityMngr) {

        // Use singleton
        if ( this.entityManager == null  ) {
            this.entityManager = entityMngr;
        } else
        if ( entityManager.equals( entityMngr ) ) {
        } else {
            this.entityManager = entityMngr;
        }
    }

    @Override
    public void save(Object entity) {
        runInTransaction(() -> entityManager.persist(entity));
    }

    @Override
    public Optional findById(Serializable serializable) {
        return Optional.empty();
    }

    @Override
    public void typeIn(Serializable serializable) {
        CRUDRepository.super.typeIn(serializable);
    }

    @Override
    public Object getById(Serializable serializable) {
        return CRUDRepository.super.getById(serializable);
    }

    @Override
    public void delete(Serializable serializable) {
        CRUDRepository.super.delete(serializable);
    }

    @Override
    public void delete(Object entity) {
        runInTransaction(() -> entityManager.remove(entity));
    }

    protected void runInTransaction(Runnable query) {
        EntityTransaction transaction = entityManager.getTransaction();
        boolean isActive = transaction.isActive();
        try {
            if (!isActive) {
                transaction.begin();
            }
            query.run();
            if (!isActive) {
                transaction.commit();
            }
        } catch (Exception e) {
            if (isActive) {
                throw e;
            } else {
                System.err.println(e.getMessage());
                transaction.rollback();
            }
        }
    }

    public abstract Collection<T> findAll();

}
