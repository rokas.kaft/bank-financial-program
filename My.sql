-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Expence_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Expence_type` (
  `expence_type_id` INT NOT NULL,
  `expence_type_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`expence_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Expence_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Expence_category` (
  `expence_category_id` INT NOT NULL,
  `expence_category_name` VARCHAR(45) NOT NULL,
  `expence_type_id` INT NOT NULL,
  `Expence_type_expence_type_id` INT NOT NULL,
  PRIMARY KEY (`expence_category_id`, `Expence_type_expence_type_id`),
  INDEX `fk_Expence_category_Expence_type1_idx` (`Expence_type_expence_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_Expence_category_Expence_type1`
    FOREIGN KEY (`Expence_type_expence_type_id`)
    REFERENCES `mydb`.`Expence_type` (`expence_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Budget`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Budget` (
  `buget_id` INT NOT NULL,
  `income_id` INT NOT NULL,
  `expence_id` INT NOT NULL,
  PRIMARY KEY (`buget_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Expence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Expence` (
  `expence_id` INT NOT NULL,
  `expence_category_id` INT NOT NULL,
  `expence_amount` FLOAT NOT NULL,
  `expence_date` DATE NOT NULL,
  `Expence_category_expence_category_id` INT NOT NULL,
  `Budget_buget_id` INT NOT NULL,
  PRIMARY KEY (`expence_id`, `Expence_category_expence_category_id`, `Budget_buget_id`),
  INDEX `fk_Expence_Expence_category1_idx` (`Expence_category_expence_category_id` ASC) VISIBLE,
  INDEX `fk_Expence_Budget1_idx` (`Budget_buget_id` ASC) VISIBLE,
  CONSTRAINT `fk_Expence_Expence_category1`
    FOREIGN KEY (`Expence_category_expence_category_id`)
    REFERENCES `mydb`.`Expence_category` (`expence_category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Expence_Budget1`
    FOREIGN KEY (`Budget_buget_id`)
    REFERENCES `mydb`.`Budget` (`buget_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Income_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Income_category` (
  `income_category_id` INT NOT NULL,
  `income_category_name` VARCHAR(45) NULL,
  PRIMARY KEY (`income_category_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Income`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Income` (
  `income_id` INT NOT NULL,
  `income_category_id` INT NOT NULL,
  `income_amount` FLOAT NOT NULL,
  `income_date` DATE NOT NULL,
  `Income_category_income_category_id` INT NOT NULL,
  `Budget_buget_id` INT NOT NULL,
  PRIMARY KEY (`income_id`, `Income_category_income_category_id`, `Budget_buget_id`),
  INDEX `fk_Income_Income_category_idx` (`Income_category_income_category_id` ASC) VISIBLE,
  INDEX `fk_Income_Budget1_idx` (`Budget_buget_id` ASC) VISIBLE,
  CONSTRAINT `fk_Income_Income_category`
    FOREIGN KEY (`Income_category_income_category_id`)
    REFERENCES `mydb`.`Income_category` (`income_category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Income_Budget1`
    FOREIGN KEY (`Budget_buget_id`)
    REFERENCES `mydb`.`Budget` (`buget_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
