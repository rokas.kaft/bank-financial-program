package coding_side.crud_repositories;

import coding_side.entity.Income;
import coding_side.entity.IncomeCategory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

public class IncomeCategoryCRUDRepository extends AbstractRepository<Integer, IncomeCategory> {

    public IncomeCategoryCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<IncomeCategory> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IncomeCategory> query = builder.createQuery(IncomeCategory.class);
        query.from(IncomeCategory.class);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
