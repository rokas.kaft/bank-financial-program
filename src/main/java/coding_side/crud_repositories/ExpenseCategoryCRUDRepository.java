package coding_side.crud_repositories;

import coding_side.entity.Expense;
import coding_side.entity.ExpenseCategory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

public class ExpenseCategoryCRUDRepository extends AbstractRepository<Integer, ExpenseCategory> {

    public ExpenseCategoryCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<ExpenseCategory> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ExpenseCategory> query = builder.createQuery(ExpenseCategory.class);
        query.from(ExpenseCategory.class);
        return entityManager.createQuery(query).getResultList();
    }
}
