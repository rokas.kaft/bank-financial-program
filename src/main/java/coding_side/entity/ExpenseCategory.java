package coding_side.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "expense_category")

public class ExpenseCategory implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "expense_category_id")
    private Integer category_Id;

    @Column (name = "expense_category_name", nullable = false)
    private String category_Name;

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    @OneToMany(mappedBy = "expenseCategory")
    private List<Expense> expenses;

    public Integer getCategory_Id() {
        return category_Id;
    }

    public void setCategory_Id(Integer category_Id) {
        this.category_Id = category_Id;
    }

    public String getCategoryName() {
        return category_Name;
    }

    public void setCategoryName(String category_Name) {
        this.category_Name = category_Name;
    }

    @Override
    public String toString() {
        return "expense_category{" +
                "category_Id=" + category_Id +
                ", category_Name='" + category_Name + '\'' +
                ", getExpenceTypes=" + getExpenses() +
                '}';
    }
}
