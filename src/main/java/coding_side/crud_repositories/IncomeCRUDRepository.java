package coding_side.crud_repositories;

import coding_side.entity.Income;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;
import java.util.List;

public class IncomeCRUDRepository extends AbstractRepository<Integer, Income> {

    public IncomeCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<Income> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Income> query = builder.createQuery(Income.class);
        query.from(Income.class);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
