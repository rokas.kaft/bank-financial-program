package coding_side.exceptions;

public class BankingAppException extends RuntimeException {

    public BankingAppException(String message) {
        super(message);
    }

    public BankingAppException(Throwable cause) {
        super(cause);
    }
}
