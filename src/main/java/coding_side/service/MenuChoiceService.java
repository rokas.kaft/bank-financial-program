package coding_side.service;

import coding_side.crud_repositories.AbstractRepository;
import coding_side.crud_repositories.ExpenseCRUDRepository;
import coding_side.crud_repositories.IncomeCRUDRepository;
import coding_side.entity.Expense;
import coding_side.entity.IEntity;
import coding_side.entity.Income;

import javax.persistence.EntityManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Supplier;

public class MenuChoiceService {

    private static Scanner scanner = new Scanner(System.in);

    // Upper bounded generics
    private static Map<Integer,? super AbstractRepository> entityMap = new HashMap<>();

    private Supplier<AbstractRepository> supplier;

    public MenuChoiceService() {
    }

    protected MenuChoiceService( final Supplier< AbstractRepository > supplier) {
        this.supplier = supplier;
    }

    AbstractRepository createContents() {
        return supplier.get();
    }

    public void addMenuChoices( final int number, final IEntity entity, final EntityManager entityManager ) {

/*

    new HashMap<String,String>();

*/
    String myString = "java.util.HashMap";

        try {

/*
            Constructor<?> hashMapConstructor = Class.forName(myString).getConstructor(null);
            System.out.println(Arrays.toString(hashMapConstructor.getParameterTypes())); // prints "[]"
            HashMap<Long,? super AbstractRepository> myMap = (HashMap<Long,AbstractRepository>)
                    hashMapConstructor.newInstance(null);
*/
            // new Map<String, String>(); // do not construct interface!

            // But we can construct instances like this:
/*          myMap.put( 1L, new IncomeCRUDRepository( entityManager ) );
            myMap.put( 2L, new ExpenseCRUDRepository( entityManager ) );
*/
            Class<?> abstractRepositoryClass =
                Class.forName(
                        String.format("coding_side.crud_repositories.%sCRUDRepository",
                                entity.getClass().getSimpleName()));

        Constructor<?> constructor = abstractRepositoryClass.getConstructor(EntityManager.class);

        System.out.println("ADD: using abstract " + constructor);
        this.entityMap.put( number, (AbstractRepository) constructor.newInstance(entityManager));

        } catch (
            ClassNotFoundException | NoSuchMethodException |
            InvocationTargetException | InstantiationException |
            IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    public void makeChoices() {

            while (true) {

                System.out.println( "Entity Map contains: "  + entityMap.size() );
                entityMap.forEach( (key, value) -> System.out.println(key + ":" + value));

                System.out.println("-------------------------\n");
                System.out.println("Pasirinkite norimą atlikti operaciją");
                System.out.println("-------------------------\n");

                System.out.println("1 - Pilnas pajamų sąrašas");
                System.out.println("2 - Pilnas pajamų kategorijų sąrašas");
                System.out.println("3 - Pilnas išlaidų sąrašas");
                System.out.println("4 - Pilnas išlaidų kategorijų sąrašas");
                System.out.println("5 - Pilnas biudžetų sąrašas\n");
                // b
                System.out.println("0 - Išeiti iš programos");

                System.out.println("Pasirinkite norimą atlikti veiksmą :");

                int choice = 0;
                try {
                    choice = scanner.nextInt();

                } catch ( InputMismatchException e ) {
                    System.err.println("Jūs pasirinkote negaliojantį pasirinkimą.");
                }

                switch (choice) {

                    case 1: case 2: case 3: case 4: case 5:
                        if ( entityMap.containsKey( choice )) {

                            System.out.println("=====================");

                            // Need to cast for the upper-bounded generics
                            ((AbstractRepository)entityMap.get(choice)).findAll().stream().forEach(System.out::println);
                            System.out.println("=====================");
                            break;
                        }

                    case 0:
                        System.out.println("=====================");
                        System.exit(0);
                        System.out.println("=====================");
                        break;

                    default:
                        System.out.println("Jūs pasirinkote netinkamą meniu punktą");
                }
            }
    }
}
