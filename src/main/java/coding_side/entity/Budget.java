package coding_side.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "budget")
public class Budget implements IEntity {

    @Id
    @GeneratedValue
    @Column (name = "budget_id")
    private Integer id;

    @Column (name = "budget_income_id")
    private Integer budgetIncomeId;

    @Column (name = "budget_expense_id")
    private Integer budgetExpenseId;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "budget_to_expense",
            joinColumns = { @JoinColumn(name = "budget_id") },
            inverseJoinColumns = { @JoinColumn(name = "expense_id") })
    private Set<Expense> expenses = new HashSet<>();

    public Set<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(Set<Income> incomes) {
        this.incomes = incomes;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "budget_to_income",
            joinColumns = { @JoinColumn(name = "budget_id") },
            inverseJoinColumns = { @JoinColumn(name = "income_id") })
    private Set<Income> incomes = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBudgetIncomeId() {
        return budgetIncomeId;
    }

    public void setBudgetIncomeId(Integer budgetIncomeId) {
        this.budgetIncomeId = budgetIncomeId;
    }

    public Integer getBudgetExpenseId() {
        return budgetExpenseId;
    }

    public void setBudgetExpenseId(Integer budgetExpenseId) {
        this.budgetExpenseId = budgetExpenseId;
    }

    public Set<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expense> expenses) {
        this.expenses = expenses;
    }

    @Override
    public String toString() {
        return "Budget{" +
                "id=" + id +
                ", budgetIncomeId=" + budgetIncomeId +
                ", budgetExpenseId=" + budgetExpenseId +
                ", expenses=" + expenses +
                ", incomes=" + incomes +
                '}';
    }
}




