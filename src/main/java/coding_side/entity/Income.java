package coding_side.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table (name = "income")

public class Income implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "income_id")
    private Integer id;

    @Column ( name = "income_category_id")
    private Integer incomeCategoryId;

    @Column (name = "income_amount", nullable = false)
    private Float incomeAmount;

    @Column (name = "income_date", nullable = false)
    private LocalDate incomeDate;

    public Set<Budget> getBudget() {
        return budget;
    }

    public void setBudget(Set<Budget> budget) {
        this.budget = budget;
    }

    @ManyToMany(mappedBy = "incomes", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Budget> budget = new HashSet<>();

    @ManyToOne(optional=false)
    @JoinColumn(name = "income_category_id",insertable=false, updatable=false)
    private IncomeCategory incomeCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncomeCategoryId() {
        return incomeCategoryId;
    }

    protected void setIncomeCategoryId(Integer incomeCategoryId) {
    }

    public Float getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(Float incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public LocalDate getIncomeDate() {
        return incomeDate;
    }

    public void setIncomeDate(LocalDate incomeDate) {
        this.incomeDate = incomeDate;
    }

    public IncomeCategory getIncomeCategory() {
        return incomeCategory;
    }

    public void setIncomeCategory(IncomeCategory incomeCategory) {
        this.incomeCategory = incomeCategory;
    }

    @Override
    public String toString() {
        return "Income{" +
                "id=" + id +
                ", incomeCategoryId=" + incomeCategoryId +
                ", incomeAmount=" + incomeAmount +
                ", incomeDate=" + incomeDate +
                ", budget=" + budget +
                ", incomeCategory=" + incomeCategory +
                '}';
    }
}
