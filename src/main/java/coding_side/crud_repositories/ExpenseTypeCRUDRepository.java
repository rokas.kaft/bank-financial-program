package coding_side.crud_repositories;

import coding_side.entity.ExpenseType;
import coding_side.entity.Income;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.Collection;

public class ExpenseTypeCRUDRepository extends AbstractRepository<Integer, ExpenseType> {

    public ExpenseTypeCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<ExpenseType> findAll() {
        return new ArrayList<>();
    }

}
