package coding_side.main;

import coding_side.crud_repositories.AbstractRepository;
import coding_side.crud_repositories.BudgetCRUDRepository;
import coding_side.entity.*;
import coding_side.service.MenuChoiceService;
import coding_site.util.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        final Session session = HibernateUtil.getSessionFactory().openSession();

        final EntityManager entityManager =
                session.getEntityManagerFactory().createEntityManager();

        entityManager.getTransaction().begin();

        // Saving the relation
        String [] allTypes = { "TYPE_20", "TYPE_30", "TYPE_50" };

        Expense [] expenses = new Expense[ 3 ];
        Arrays.setAll(expenses, expense -> new Expense());

        AtomicInteger expense_counter = new AtomicInteger();
        Arrays.asList( allTypes ).forEach( type -> {

            ExpenseType expenseType = new ExpenseType();
            expenseType.setType_Name( type );

            ExpenseCategory expenseCategory = new ExpenseCategory();
            expenseCategory.setCategoryName( "EXP_CAT_" + type );

            expenses[ expense_counter.get() ].setExpenseCategory( expenseCategory );
            expenses[ expense_counter.get() ].setExpenseAmount( 300 * expense_counter.get() );
            expenses[ expense_counter.get() ].setExpenseDate(
                    new Date( Instant.now().toEpochMilli() ).toLocalDate() );

            session.save( expenseCategory );
            session.save( expenses[ expense_counter.get() ] );

            expenseType.setExpenceCategory( expenseCategory );

            // Add expense type object
            session.save( expenseType );

            expense_counter.getAndIncrement();
        }  );

        // Saving the relation
        Income [] incomes = new Income[ 8 ];
        Arrays.setAll( incomes, income -> new Income() );

        AtomicReference<Integer> amounts = new AtomicReference<>(0);
        Arrays.asList( incomes ).forEach( income -> {

            amounts.set( amounts.get() + 100 );
            IncomeCategory incomeCategory = new IncomeCategory();
            incomeCategory.setIncomeCategoryName( "INC_CAT_" + amounts.get() );

            // Add income category object
            session.save( incomeCategory );

            income.setIncomeDate( new Date( Instant.now().toEpochMilli() ).toLocalDate() );
            income.setIncomeAmount( amounts.get().floatValue() );
            income.setIncomeCategory( incomeCategory );

            // Add income type object
            session.save( income );

        }  );
        entityManager.getTransaction().commit();

        Budget budget = new Budget();
        budget.setBudgetIncomeId( incomes[0].getId() );

        budget.setIncomes(
                Arrays.stream(incomes).filter( income ->
                        income.getIncomeCategory().getIncomeCategoryName().startsWith("EXP_CAT")
                ).collect(Collectors.toSet()));

        budget.setExpenses(
                Arrays.stream(expenses).filter( expense ->
                        expense.getExpenseCategory().getCategoryName().startsWith("EXP_CAT")
                ).collect(Collectors.toSet()));

        AbstractRepository<Integer, Budget> budgetCRUDRepository
                = new BudgetCRUDRepository( entityManager );
        // Save Budget type object
        budgetCRUDRepository.save( budget );

        MenuChoiceService menuChoice = new MenuChoiceService();

        menuChoice.addMenuChoices( 1, new Income(), entityManager );
        menuChoice.addMenuChoices( 2, new IncomeCategory(), entityManager );
        menuChoice.addMenuChoices( 3, new Expense(), entityManager );
        menuChoice.addMenuChoices( 4, new ExpenseCategory(), entityManager );
        menuChoice.addMenuChoices( 5, budget, entityManager );
        // and so on ...

        menuChoice.makeChoices();
        entityManager.close();

    }
}
