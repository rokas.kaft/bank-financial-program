package coding_side.crud_repositories;

import coding_side.entity.Expense;
import coding_side.entity.ExpenseType;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

public class ExpenseCRUDRepository extends AbstractRepository<Integer, Expense> {

    public ExpenseCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<Expense> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Expense> query = builder.createQuery(Expense.class);
        query.from(Expense.class);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
