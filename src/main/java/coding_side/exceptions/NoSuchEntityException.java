package coding_side.exceptions;

public class NoSuchEntityException extends BankingAppException{
    public NoSuchEntityException(String message) {
        super(message);
    }

    public NoSuchEntityException(Throwable cause) {
        super(cause);
    }
}
