package coding_side.entity;

import javax.persistence.*;

@Entity
@Table(name = "expense_type")
public class ExpenseType implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "expense_type")
    private Integer type_Id;

    @Column (name = "expense_type_name", nullable = false)
    private String type_Name;

    @ManyToOne
    @JoinColumn(name = "expense_id")
    private ExpenseCategory expenseCategory;

    public Integer getType_Id() {
        return type_Id;
    }

    public void setType_Id(Integer type_Id) {
        this.type_Id = type_Id;
    }

    public String getType_Name() {
        return type_Name;
    }

    public void setType_Name(String type_Name) {
        this.type_Name = type_Name;
    }

    public ExpenseCategory getExpenceCategory() {
        return expenseCategory;
    }

    public void setExpenceCategory(ExpenseCategory expenceCategory) {
        this.expenseCategory = expenceCategory;
    }

    @Override
    public String toString() {
        return "ExpenseType{" +
                "type_Id=" + type_Id +
                ", type_Name='" + type_Name + '\'' +
                ", expenceCategory=" + expenseCategory +
                '}';
    }
}
