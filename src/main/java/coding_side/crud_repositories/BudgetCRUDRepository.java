package coding_side.crud_repositories;

import coding_side.entity.Budget;
import coding_side.entity.Income;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;
import java.util.Optional;

public class BudgetCRUDRepository extends AbstractRepository<Integer, Budget> {

    public BudgetCRUDRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Collection<Budget> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Budget> query = builder.createQuery(Budget.class);
        query.from(Budget.class);
        return entityManager.createQuery(query).getResultList();
    }



    public Optional<Budget> save(Budget budget) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(budget);
            entityManager.getTransaction().commit();
            return Optional.of(budget);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.empty();

    }
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

}
